// ==UserScript==
// @name         ArenaAdditionalInfo
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  try to take over the world!
// @author       Shadilan
// @updateURL    https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/ArenaAdditionalInfo.user.js  
// @downloadURL  https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/ArenaAdditionalInfo.user.js  
// @match        http*://*.the-reincarnation.com/cgi-bin/arena2.cgi*
// @grant        none
// ==/UserScript==

var landCount=document.getElementsByClassName('t9')[0].getElementsByClassName('s2')[0].innerHTML.replace(' Acres','');
var dopInfo=document.createElement("table");
dopInfo.id="AdditionalInfo";
document.body.insertBefore(document.createElement("br"),document.getElementsByClassName("footer")[0]);
document.body.insertBefore(dopInfo,document.getElementsByClassName("footer")[0]);

var row1=document.createElement("tr");
row1.id="ArmyPower";
row1.innerHTML="<td><b>Army Needed</b></td>";
dopInfo.append(row1);
var row2=document.createElement("tr");
row2.innerHTML="<td><b>Siege</b></td><td>"+(landCount*5)+"</td>";
dopInfo.append(row2);
var row3=document.createElement("tr");
row3.innerHTML="<td><b>Regular</b></td><td>"+(landCount*5/2)+"</td>";
dopInfo.append(row3);
