// ==UserScript==
// @name         PowerRank With Chart
// @namespace    http://tampermonkey.net/
// @version      0.8
// @description  Show Rank info and activity chart
// @author       You
// @updateURL    https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/PowerRankWithChart.user.js  
// @downloadURL  https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/PowerRankWithChart.user.js  
// @match        http*://*.the-reincarnation.com/cgi-bin/rank.cgi*
// @grant        none
// ==/UserScript==
var infoDiv=document.createElement("div");
infoDiv.id="chartContainer";
infoDiv.style="height:300px; width:100%; display:none;";
document.body.appendChild(infoDiv);
var scr=document.createElement("script");
scr.type="text/javascript";
scr.src="https://canvasjs.com/assets/script/canvasjs.min.js";
document.head.appendChild(scr);
var chart=function (a,b) {
	infoDiv.style="height:300px; width:100%; display:block;";
	datapoints=[];
    landpoints=[];
    var minp=10000000;
    var date = new Date();
    var last = new Date(date.getTime() - (2 * 24 * 60 * 60 * 1000));
	for (var i=0;i<b.length;i++){


        if (new Date(b[i].date)>last) {
            datapoints.push({x:new Date(b[i].date),y: Number(b[i].mpower)});
            if (!isNaN(b[i].mland))                landpoints.push({x:new Date(b[i].date),y: Number(b[i].mland)*1000});
            else landpoints.push({x:new Date(b[i].date),y: 0});
            if (minp>Number(b[i].mpower)) {
                 minp=Number(b[i].mpower);
            }
            if (minp>Number(b[i].mland)*1000) minp=Number(b[i].mland)*1000;
        }


	}
    console.log(landpoints);
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: a+" Power"
    },
    axisX:{
        minimum:datapoints[0].x,
        maximum: date,
        title: "timeline",
        gridThickness: 1
    },
    axisY: {
        title: "Power",
        minimum:minp/2
    },
    data: [{
		name: "Power",
		showInLegend: true,
		legendMarkerType: "square",
		type: "area",
        color: "rgba(0,75,141,0.7)",
        markerColor:"blue",
		dataPoints: datapoints
	},
	{
		name: "Land",
		showInLegend: true,
		legendMarkerType: "square",
		type: "area",
		color: "rgba(0,255,70,1",
        yValueFormatString:  "#0,.",
		markerColor:"green",
		dataPoints: landpoints}]
    
});

    chart.render();
};

var selfrow=document.getElementsByClassName('myself')[0];
var sland=parseInt(selfrow.getElementsByClassName('t4')[0].innerHTML);
var sfort=parseInt(selfrow.getElementsByClassName('t5')[0].innerHTML);
var spower=selfrow.getElementsByClassName('t6')[0].innerHTML;
var mxpower=spower*125/100;
var mnpower=spower*80/100;
var ssl=0;
var tenDays=new Date((new Date()).getTime() - (2 * 24 * 60 * 60 * 1000));

var k=selfrow.getElementsByClassName('t2')[0].getElementsByTagName("img")[0].title;
if (k=='ERADICATION') ssl=477;
else if (k=='PHANTASM') ssl=631;
else if (k=='ASCENDANT') ssl=456;
else if (k=='VERDANT') ssl=463;
else if (k=='NETHER') ssl=477;
var spow=0;
var spow=Math.round(spower-(sfort*19360+sland*1000+ssl*1000+6500*sland/40));
var sresp=Math.round(spow*100/power);

var highlightRange =function()
{

    var tpower=this.getElementsByClassName('t6')[0].innerHTML;
    var tmx=tpower*125/100;
    var tmn=tpower*80/100;
    var crows = document.getElementById("ranktable").getElementsByTagName("tr");
    console.log("1");
    var mmname=this.getElementsByClassName('t3')[0].getElementsByTagName('a')[0].innerHTML;
    for (var k=0;k<newInfo.length;k++){
        if (newInfo[k].mname==mmname) {
            chart(mmname,newInfo[k].powerlist);
            break;
        }

    }

    for (var i=0;i<crows.length;i++)
    {
	    if (rows[i].getElementsByClassName('t6').length>0){
		    var power=0;
		    power=rows[i].getElementsByClassName('t6')[0].innerHTML;
            if (power<tmx && power>tmn) {
                rows[i].getElementsByClassName('t1')[0].style.color="MediumSeaGreen";
                rows[i].getElementsByClassName('t1')[0].style.fontWeight = 'bold';
            }
            else {
                rows[i].getElementsByClassName('t1')[0].style.color="#b6b6b6";
                rows[i].getElementsByClassName('t1')[0].style.fontWeight = '';
            }
	    }
    }

};

var s=localStorage.getItem('MagesInfo');
var oldInfo=[];
if (s!==undefined && s!==''){
    oldInfo=JSON.parse(s);
}
s=localStorage.getItem('Counters');
var counters=[];
if (s!==undefined && s!==''){
    counters=JSON.parse(s);
}
var newInfo=[];
var head=document.getElementById('ranktable').getElementsByTagName("tr")[0];
var td=document.createElement("td");
td.innerHTML=" Difference ";
td.style.padding='0px 10px 0px';
head.appendChild(td);
td=document.createElement("td");
td.innerHTML=" % ";
td.style.padding='0px 10px 0px';
head.appendChild(td);
if (oldInfo!==undefined && oldInfo!==null){
td=document.createElement("td");
td.innerHTML=" Changes ";
td.style.padding='0px 10px 0px';
head.appendChild(td);

td=document.createElement("td");
td.innerHTML="  ";
td.style.padding='0px 10px 0px';
head.insertBefore(td,head.getElementsByTagName('td')[4]);
}

var rows = document.getElementById("ranktable").getElementsByTagName("tr");
for (var i=0;i<rows.length;i++)
{
	if (rows[i].getElementsByClassName('t4').length>0){
		var powerlist=[];
        rows[i].onclick=highlightRange;
		var land=0;
		land=parseInt(rows[i].getElementsByClassName('t4')[0].innerHTML);
		var fort=0;
		fort=parseInt(rows[i].getElementsByClassName('t5')[0].innerHTML);
		var power=0;
		power=rows[i].getElementsByClassName('t6')[0].innerHTML;
        var mname=rows[i].getElementsByClassName('t3')[0].getElementsByTagName('a')[0].innerHTML;


        if (counters!==null && counters!==undefined){
            for (var cn=0;cn<counters.length;cn++){
                if (counters[cn].Name==mname){
                    rows[i].getElementsByClassName('t3')[0].getElementsByTagName('a')[0].style.color='MediumSeaGreen';
                    rows[i].getElementsByClassName('t3')[0].getElementsByTagName('a')[0].title='('+counters[cn].Counter+'% '+counters[cn].Tm+')';
                }
            }
        }
        if (power<mxpower && power>mnpower) rows[i].getElementsByClassName('t6')[0].style.color="MediumSeaGreen";
		var t=0;
		var k=rows[i].getElementsByClassName('t2')[0].getElementsByTagName("img")[0].title;
		if (k=='ERADICATION') t=477;
		else if (k=='PHANTASM') t=631;
		else if (k=='ASCENDANT') t=456;
		else if (k=='VERDANT') t=463;
		else if (k=='NETHER') t=477;
		var resp=Math.round((power-(fort*19360+land*1000+t*1000+6500*land/40))*100/power);
        var td=document.createElement("td");
        var cpow=spow-Math.round(power-(Math.min(fort,land*0.025)*19360+land*1000+t*1000+6500*land/40));
		td.innerHTML=" "+cpow.toLocaleString('ru-RU', { useGrouping: 'true'})+" ";
        td.style.padding='0px 10px 0px';
        if (cpow>spow*30/100) {
            td.style.color = "MediumSeaGreen";
        }
        else if (cpow>0) {

            td.style.color = "yellow";
        }
		rows[i].appendChild(td);
		var td=document.createElement("td");
		td.innerHTML=" "+resp+" ";
        td.style.padding='0px 10px 0px';
        if (resp<30) {

            td.style.color = "MediumSeaGreen";
        }
        else if (resp<50) {

            td.style.color = "yellow";
        }


		rows[i].appendChild(td);
        td=document.createElement("td");
        td.innerHTML=" ? ";
        var tdland=document.createElement('td');
        tdland.class='t4';
        if (oldInfo!==undefined && oldInfo!==null){
            for (var mg=0;mg<oldInfo.length;mg++){
                if (oldInfo[mg].mname==mname){
                    td.innerHTML=" "+(power-oldInfo[mg].mpower).toLocaleString('ru-RU', { useGrouping: 'true'});
                    if (oldInfo[mg].mland!==undefined  &&  oldInfo[mg].mland!==null && land!=oldInfo[mg].mland)
                    {
                        if (land-oldInfo[mg].mland>0){
                            tdland.innerHTML="<font color='MediumSeaGreen'>"+(land-oldInfo[mg].mland)+"</font>";
                        }
                        else {
                            tdland.innerHTML="<font color='red'>"+(land-oldInfo[mg].mland)+"</font>";
                        }

                    }

                    if (oldInfo[mg].powerlist!==undefined && oldInfo[mg].powerlist!==null){
                        powerlist=oldInfo[mg].powerlist;
                        powerlist.push({'date':new Date(),'mpower':power,'mland':land});
                    } else {
                        powerlist.push({'date':new Date(),'mpower':power,'mland':land});
                    }
                    for (var pli=0;pli<powerlist.length-1;pli++){
                        //console.log(powerlist[pli]);
                        if (powerlist[pli]!=undefined && new Date(powerlist[pli].date)<tenDays) {
                            //console.log(powerlist[pli]);
                            powerlist.splice(pli,1);
                        } else if (i>0 && powerlist[pli]!=undefined && powerlist[pli-1]!=undefined && powerlist[pli].mpower==powerlist[pli-1].mpower && powerlist[pli].mland==powerlist[pli-1].mland)
                        {
                            powerlist.splice(pli,1);
                        }
                    }
                }
            }

            td.style.padding='0px 10px 0px';
            rows[i].appendChild(td);
        }
        newInfo.push({
          'mname'  : mname,
          'mpower': power,
          'mland':land,
          'powerlist':powerlist
        });
        rows[i].insertBefore(tdland,rows[i].getElementsByTagName('td')[4]);


	}
}
var resultLog=[];
if (oldInfo!=null)
for (var i=0;i<oldInfo.length;i++){
    var res=0;
    for (var j=0;j<newInfo.length;j++){
        if (oldInfo[i].mname==newInfo[j].mname) {res=1;break;}
    }
    if (res===0) resultLog.push(oldInfo[i]);
}
for (var j=0;j<newInfo.length;j++){
        resultLog.push(newInfo[j]);
    }
 localStorage.setItem('MagesInfo', JSON.stringify(resultLog));
var hide=true;
var hideD=function(){

    var rows = document.getElementById("ranktable").getElementsByTagName("tr");
    for (var i=0;i<rows.length;i++){
        if (rows[i].getElementsByClassName('t9').length>0){
            if (rows[i].getElementsByClassName('t9')[0].innerHTML=='D'){
                if (hide) rows[i].style.display="none";
                else rows[i].style.display="table-row";
            }
        }
    }
    hide=!hide;
    if (hide) btn.innerHTML='Hide D';
    else btn.innerHTML='Show D';
};

var btn=document.createElement('button');
btn.onclick=hideD;
btn.title='Hide D';
btn.innerHTML='Hide D';
document.getElementsByClassName('footer')[0].appendChild(btn);

