// ==UserScript==
// @name         UnderAttack
// @namespace    http://tampermonkey.net/
// @version      0.5
// @description  try to take over the world!
// @updateURL    https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/UnderAttack.user.js  
// @downloadURL  https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/UnderAttack.user.js  
// @author       Shadilan
// @match        http*://*.the-reincarnation.com/cgi-bin/*
// @grant        none
// ==/UserScript==
if (document.getElementById('status')!==null)
{
var arr=document.getElementById('status').getElementsByClassName('tooltiptext');
var uA=0;
var uG=0;
for (var i=0;i<arr.length;i++)
{
    if (arr[i].innerHTML.indexOf('Meteor Storm')!=-1) uA++;
    if (arr[i].innerHTML.indexOf('Death and Decay')!=-1) uA++;
    if (arr[i].innerHTML.indexOf('Confuse')!=-1) uA++;
    if (arr[i].innerHTML.indexOf('Call Lightning')!=-1) uA++;
    if (arr[i].innerHTML.indexOf('Pacifism')!=-1) uG++;
    if (arr[i].innerHTML.indexOf('Locust Swarm')!=-1) uG++;
}
if (uA>0)
    document.getElementsByTagName('body')[0].style.background='DarkRed';
else if (uG>0)
    document.getElementsByTagName('body')[0].style.background='DarkGreen';
}