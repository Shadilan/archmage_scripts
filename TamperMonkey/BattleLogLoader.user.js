// ==UserScript==
// @name         BattleLogLoader
// @namespace    http://your.homepage/
// @version      0.2
// @description  enter something useful
// @author       You
// @updateURL    https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/BattleLogLoader.user.js  
// @downloadURL  https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/BattleLogLoader.user.js  
// @match        http://*.the-reincarnation.com/cgi-bin/viewbattle.cgi?b=*
// @grant         GM_xmlhttpRequest
// ==/UserScript==

var logurl=document.URL;

var battleid=logurl.substring(logurl.indexOf('b=')+2);
var servername=logurl.substring(7,logurl.indexOf('.the'));
var bodytext=document.body.innerHTML;
//define attackType
var attackType='U';
if (bodytext.indexOf('You blocked')>0) attackType='DS';
else if (bodytext.indexOf('Your attack failed')>0) attackType='AF';
else if (bodytext.indexOf('You failed to defend your country')>0) attackType='DF';
else if (bodytext.indexOf('Your attack was successful')>0) attackType='AS';
else if (bodytext.indexOf('Your enemy was too weak')>0) attackType='AS';
else if (bodytext.indexOf('They pillaged')>0) attackType='AS';
else if (bodytext.indexOf('In your hasty retreat')>0) attackType='AF';
else if (bodytext.indexOf('You estimated you lost')>0) attackType='DF';
//define attacker
var attacker=document.getElementsByClassName('battle')[0].innerHTML;
//define defender
var defender=document.getElementsByClassName('battle')[1].innerHTML;
bodytext=bodytext.replace('<body><a class="topbanner" href="mainmenu.cgi"></a>','');
bodytext=bodytext.replace('<h1>RENUNTIO INSULTUS</h1>','');
bodytext=bodytext.replace('<img src="http://images.the-reincarnation.com/i0000.jpg" border="0">','');
			bodytext=bodytext.replace('<div class="f1"><a href="war.cgi"><span>War</span></a></div>','');
			bodytext=bodytext.replace('<div class="f2"><a href="mainmenu.cgi"><span>Mainmenu</span></a></div>','');
			bodytext=bodytext.replace('<div class="f3"><a href="rank.cgi"><span>Ranklist</span></a></div>','');

var tab1='<table>'+document.getElementsByClassName('tdTwo tdCenter')[0].innerHTML+'</table>';
var tab2='<table>'+document.getElementsByClassName('tdTwo tdCenter')[1].innerHTML+'</table>';


var SendRequest=function()
{
    var send_req='attacker='+attacker+ '&defender='+defender+ '&servername='+servername+ '&attackType='+attackType +'&logid='+battleid+ '&logtext='+bodytext+'&tab1='+tab1+'&tab2='+tab2;
    GM_xmlhttpRequest({
            method: "POST",
            url: "http://archmage.esy.es/newgetdata.php",
            data: send_req,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            onload: function(response) {
                if (response.responseText=='Error') {alert('Ошибка отправки данных');}
                console.log(response.responseText);               
            }
        }); 
    
};
SendRequest();