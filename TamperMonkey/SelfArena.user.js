// ==UserScript==
// @name         SelfArena
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Archmage Arena
// @author       You
// @updateURL    https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/SelfArena.user.js
// @downloadURL  https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/SelfArena.user.js 
// @match        http*://*.the-reincarnation.com/cgi-bin/arena2.cgi?id=136
// @grant        none
// ==/UserScript==

var powerStr=document.getElementsByClassName('t8')[0].getElementsByClassName('s2')[0].innerHTML.replace(/,/g,'');
var power=parseInt(powerStr.replace(/\D/g, ""),10);
var landStr=document.getElementsByClassName('t9')[0].getElementsByClassName('s2')[0].innerHTML.replace(' Acres','');
var land=parseInt(landStr.replace(/\D/g, ""),10);
var fortStr=document.getElementsByClassName('t10')[0].getElementsByClassName('s2')[0].innerHTML;
var fort=parseInt(fortStr.replace(/\D/g, ""),10);
var sl=500;
var col=document.getElementsByClassName('t6')[0].getElementsByClassName('s2')[0].innerHTML.replace(' Magic','');
		if (col=='ERADICATION') sl=477;
		else if (col=='PHANTASM') sl=631;
		else if (col=='ASCENDANT') sl=456;
		else if (col=='VERDANT') sl=463;
		else if (col=='NETHER') sl=477;
var resp=Math.round((power-(fort*19360+land*1000+sl*1000+6500*land/40))*100/power);
var rep=(power-(fort*19360+land*1000+sl*1000+6500*land/40));
localStorage.setItem('SelfUnknownPower', rep);
var tr=document.createElement("tr");
var td=document.createElement("td");
td.innerHTML=rep.toLocaleString('ru-RU', { useGrouping: 'true'});
//td.innerHTML=powerStr;
tr.appendChild(td);
td=document.createElement("td");
td.innerHTML=resp.toLocaleString('ru-RU', { useGrouping: 'true'});
tr.appendChild(td);
document.getElementById('result').appendChild(tr);