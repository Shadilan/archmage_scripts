// ==UserScript==
// @name         War
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @updateURL    https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/War.user.js  
// @downloadURL  https://gitlab.com/Shadilan/archmage_scripts/raw/master/TamperMonkey/War.user.js  
// @match        http*://*.the-reincarnation.com/cgi-bin/war.cgi
// @grant        none
// ==/UserScript==

var sup=localStorage.getItem('SelfUnknownPower');
var counting=-1;
var counter=-1;
var randomc=-1;
var countingTab;
var counterTab;
var randomTab;
var spans=document.getElementsByTagName('form')[0].getElementsByTagName('span');
var tabs=document.getElementsByTagName('form')[0].getElementsByTagName('table');
for (var i=0;i<spans.length;i++)
{
	if (spans[i].innerHTML=='Hits Counting Towards Damaged Status'){
	   counting=i;
	   countingTab=tabs[i];
	}
	else if (spans[i].innerHTML=='Counters'){
	   counter=i;
	   counterTab=tabs[i];
	}
	else if (spans[i].innerHTML=='Random Targets'){
	   randomc=i;
	   randomTab=tabs[i];
	}

}
if (counter>0){
var rows = counterTab.getElementsByTagName('tr');
    var cntrs=new Array();

for (var i=1;i<rows.length;i++)
{
	var land=0;
		land=parseInt(rows[i].getElementsByTagName('td')[3].getElementsByTagName('label')[0].innerHTML);
		var fort=0;
		fort=parseInt(rows[i].getElementsByTagName('td')[4].getElementsByTagName('label')[0].innerHTML);
		var power=0;
		power=parseInt(rows[i].getElementsByTagName('td')[5].getElementsByTagName('label')[0].innerHTML);
        var cntr=0;
        cntr=parseFloat(rows[i].getElementsByTagName('td')[10].getElementsByTagName('label')[0].innerHTML.replace('%',''));
        var name=rows[i].getElementsByTagName('td')[2].getElementsByTagName('a')[0].innerHTML;
        var tm=rows[i].getElementsByTagName('td')[11].getElementsByTagName('label')[0].innerHTML;
        if (cntr>0) {
            cntrs.push({Name:name,Counter:cntr,Tm:tm});
            rows[i].getElementsByTagName('td')[10].getElementsByTagName('label')[0].style.color="MediumSeaGreen";
            rows[i].getElementsByTagName('td')[10].getElementsByTagName('label')[0].style.fontWeight = 'bold';
        }
		var t=0;
		var k=rows[i].getElementsByTagName('td')[1].getElementsByTagName("img")[0].title;
		if (k=='ERADICATION') t=477;
		else if (k=='PHANTASM') t=631;
		else if (k=='ASCENDANT') t=456;
		else if (k=='VERDANT') t=463;
		else if (k=='NETHER') t=477;
        var rep=(power-(fort*19360+land*1000+t*1000+6500*land/40));
		var resp=Math.round(rep*100/power);
        var td=document.createElement("td");
		td.innerHTML=rep.toLocaleString('ru-RU', { useGrouping: 'true'});
		rows[i].appendChild(td);
        var td=document.createElement("td");
		td.innerHTML=(sup-rep).toLocaleString('ru-RU', { useGrouping: 'true'});
        if (sup-rep>sup*30/100) {
            td.style.color = "MediumSeaGreen";
        }
        else if (sup-rep>0) {
            td.style.color = "yellow";
        }
		rows[i].appendChild(td);
		var td=document.createElement("td");
		td.innerHTML=resp;
        if (resp<30) td.style.color = "green";
        else if (resp<50) td.style.color = "yellow";
		rows[i].appendChild(td);
}
    localStorage.setItem('Counters', JSON.stringify(cntrs));
}
if (randomc>0){
var rows = randomTab.getElementsByTagName('tr');
for (var i=1;i<rows.length;i++)
{
	var land=0;
		land=parseInt(rows[i].getElementsByTagName('td')[3].getElementsByTagName('label')[0].innerHTML);
		var fort=0;
		fort=parseInt(rows[i].getElementsByTagName('td')[4].getElementsByTagName('label')[0].innerHTML);
		var power=0;
		power=parseInt(rows[i].getElementsByTagName('td')[5].getElementsByTagName('label')[0].innerHTML);
		var t=0;
		var k=rows[i].getElementsByTagName('td')[1].getElementsByTagName("img")[0].title;
		if (k=='ERADICATION') t=477;
		else if (k=='PHANTASM') t=631;
		else if (k=='ASCENDANT') t=456;
		else if (k=='VERDANT') t=463;
		else if (k=='NETHER') t=477;
        var rep=(power-(fort*19360+land*1000+t*1000+6500*land/40));
		var resp=Math.round(rep*100/power);
        var td=document.createElement("td");
		td.innerHTML=rep.toLocaleString('ru-RU', { useGrouping: 'true'});
		rows[i].appendChild(td);
        var td=document.createElement("td");
		td.innerHTML=(sup-rep).toLocaleString('ru-RU', { useGrouping: 'true'});
        if (sup-rep>sup*30/100) {
            td.style.color = "MediumSeaGreen";
        }
        else if (sup-rep>0) {
            td.style.color = "yellow";
        }
		rows[i].appendChild(td);
		var td=document.createElement("td");
		td.innerHTML=resp;
        if (resp<30) td.style.color = "green";
        else if (resp<50) td.style.color = "yellow";
		rows[i].appendChild(td);
}
}
